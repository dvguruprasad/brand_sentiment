package sentimentanalysis.common;

/**
 * Created with IntelliJ IDEA.
 * User: abhisheg
 * Date: 26/10/12
 * Time: 2:21 PM
 */
public class Sentiment {
    Percentage percentage;
    Tweets tweets;


    public Percentage getPercentage() {
        return percentage;
    }

    public void setPercentage(Percentage percentage) {
        this.percentage = percentage;
    }

    public Tweets getTweets() {
        return tweets;
    }

    public void setTweets(Tweets tweets) {
        this.tweets = tweets;
    }

    public static class Percentage {
        float positive;
        float negative;
        float neutral;

        public Percentage() {
        }

        public Percentage(float positive, float negative, float neutral) {
            this.positive = positive;
            this.negative = negative;
            this.neutral = neutral;
        }

        public float getPositive() {
            return positive;
        }

        public void setPositive(float positive) {
            this.positive = positive;
        }

        public float getNegative() {
            return negative;
        }

        public void setNegative(float negative) {
            this.negative = negative;
        }

        public float getNeutral() {
            return neutral;
        }

        public void setNeutral(float neutral) {
            this.neutral = neutral;
        }
    }

    public static class Tweets {
        public Tweets() {
        }

        public Tweets(int positive, int negative, int neutral, int total) {
            this.positive = positive;
            this.negative = negative;
            this.neutral = neutral;
            this.total = total;
        }

        int positive;
        int negative;
        int neutral;
        int total;

        public int getPositive() {
            return positive;
        }

        public void setPositive(int positive) {
            this.positive = positive;
        }

        public int getNegative() {
            return negative;
        }

        public void setNegative(int negative) {
            this.negative = negative;
        }

        public int getNeutral() {
            return neutral;
        }

        public void setNeutral(int neutral) {
            this.neutral = neutral;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }
    }

}



