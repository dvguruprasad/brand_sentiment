package sentimentanalysis.ws;

import sentimentanalysis.common.Sentiment;

import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: abhisheg
 * Date: 26/10/12
 * Time: 12:32 PM
 */
public class Entity {
    String entity;
    Sentiment sentiment;
    HashMap<String, Long> unique_users;
    HashMap<String, Long> repeat_users;

    List<Tag> tags;

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public Sentiment getSentiment() {
        return sentiment;
    }

    public void setSentiment(Sentiment sentiment) {
        this.sentiment = sentiment;
    }

    public HashMap<String, Long> getUnique_users() {
        return unique_users;
    }

    public void setUnique_users(HashMap<String, Long> unique_users_map) {
        this.unique_users = unique_users_map;
    }

    public HashMap<String, Long> getRepeat_users() {
        return repeat_users;
    }

    public void setRepeat_users(HashMap<String, Long> repeat_users_map) {
        this.repeat_users = repeat_users_map;
    }
}
