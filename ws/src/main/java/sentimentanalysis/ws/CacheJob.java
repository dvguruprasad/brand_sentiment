package sentimentanalysis.ws;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import sentimentanalysis.common.Brand;

/**
 * Created with IntelliJ IDEA.
 * User: abhisheg
 * Date: 06/11/12
 * Time: 6:28 PM
 */
public class CacheJob extends QuartzJobBean {

    @Autowired
    private CacheTask dataTask;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println(" ------------- ");
        Brand[] brands = Brand.values();
        for (Brand brand : brands) {
             DataCache.put(brand,dataTask.getData(brand.name()));
        }
    }

    @Autowired
    public void setDataTask(CacheTask dataTask) {
        this.dataTask = dataTask;
    }
}
