package sentimentanalysis.ws;

import sentimentanalysis.common.Brand;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: abhisheg
 * Date: 06/11/12
 * Time: 5:53 PM
 */
public class DataCache {
    private static Map<Brand, Entity> cache = new ConcurrentHashMap<Brand, Entity>();


    public static void put(Brand brand, Entity entity) {
        cache.put(brand, entity);
    }

    public static Entity get(Brand brand) {
        return cache.get(brand);
    }
}
